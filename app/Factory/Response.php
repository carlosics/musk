<?php

namespace Musk\Factory;

class Response 
{
    public function __construct($response)
    {
        $this->response = $response;
        return $this;
    }

    public function code($code)
    {
        $this->response->status($code);
        return $this;
    }

    public function json(Array $data)
    {
        $this->response->end(json_encode($data));
        return $this;
    }

    public function transport($value) 
    {
        $this->transport = $value;
        return $this;
    }

    public function recovery()
    {
        return $this->transport;
    }

    public function text(string $text)
    {
        $this->response->end($text);
        return $this;
    }
}