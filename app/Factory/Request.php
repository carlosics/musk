<?php

namespace Musk\Factory;

class Request
{
    public function __construct($request) 
    {
        $this->request = $request;
    }

    public function getHeader() 
    {
        return $this->request->header;
    }

    public function getServer()
    {
        return $this->request->server;
    }

    public function get()
    {
        return (object) $this->request->get;
    }

    public function post()
    {
        return $this->request->post;
    }

    public function getCookie()
    {
        return $this->request->cookie;
    }

    public function getFiles()
    {
        return $this->request->files;
    }

    public function getBody()
    {
        return $this->request->rawContent();
    }

    public function getContent()
    {
        return $this->request->getContent();
    }

    public function getData()
    {
        return $this->request->getData();
    }
}