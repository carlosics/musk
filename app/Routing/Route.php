<?php

namespace Musk\Routing;

use Musk\Middleware\Middleware;
use Musk\Http\Response as HtR;
use Musk\Factory\Request;
use Musk\Factory\Response;

class Route
{
    public $routes = [];
    public $cache  = [];
  
    public function bind($method,$uri,$middlewares = [],$controller) 
    {
        $method = strtoupper($method);
        if (!isset($this->routes[$method])) {
            $this->routes[$method] = [];
        }

        $uri = substr($uri, 0, 1) !== '/' ? '/' . $uri : $uri;
        $uri = $this->fixURI($uri);

        $this->routes[$method][$uri]['controller']  = [$controller];
        $this->routes[$method][$uri]['middlewares'] = $middlewares;

        return $this;
    }

    public function fixURI($uri) 
    {
        $patterns = [
            '/{([A-z-]+)}/' => '(?<$1>[A-z0-9_-]+)',
            '/{([A-z-]+):(.*)}/' => '(?<$1>$2)',
        ];
        
        return preg_replace(array_keys($patterns), array_values($patterns), $uri);
    }
  
    public function run($before = [],$after = [],$method, $uri, $request, $response)
    {
        $method = strtoupper($method);
        $response = new Response($response);

        if (!isset($this->routes[$method])) {
            return HtR::pageNotFound($response);
        }

        $stack      = [];
        $controller = null;

        // monta a stack
        foreach ($this->routes[$method] as $route => $params) {
            if (preg_match("#$route#", $uri, $parameters)) {
                array_shift($parameters);
                $controller  = $params['controller'];
                $middlewares = $params['middlewares'];
                $stack = array_merge($before,$middlewares,$after);
            }
        }

        // execucao da stack
        if ($controller == null || empty($stack)) {
            return HtR::pageNotFound($response);
        }

        return (new Middleware($controller,...$stack))->handle(new Request($request),$response);
    }
}