<?php


use Swoole\Http\Server;

class Router
{
    private $routes = [];
  
    public function on($method, $uri, $callback) 
    { 
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            $this->routes[$method] = [];
        }

        $uri = substr($uri, 0, 1) !== '/' ? '/' . $uri : $uri;
        //$pattern = str_replace('/', '\/', $uri);
        //$route = '/^' . $pattern . '$/';

        $this->routes[$method][$uri] = $callback;

        return $this;
    }
  
    public function run($method, $uri)
    {
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            return null;
        }

        foreach ($this->routes[$method] as $route => $callback) {

            if (preg_match("#$route#", $uri, $parameters)) {
                array_shift($parameters);
                return call_user_func_array($callback, $parameters);
            }
        }
        return null;
    }

    public function getRoutes() 
    {
        return $this->routes;
    }
}

class OutPutJson
{
    public function json($json) 
    {
        return json_encode($json);
    }
}

class ControllerBase
{
    public $output;
    protected function json($data = []) 
    {
        return (new OutPutJson())->json($data);
    }
}

class UserController extends ControllerBase
{
    public function index() 
    {
        $params = func_get_args();

        return $this->json($params);
    }
}

class SSwoole extends Router
{
    public function __construct($ip = "0.0.0.0",$port = 9501)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->http = new Server($ip, $port);
        return $this;
    }

    private function up() 
    {
        $ip = $this->ip;
        $port = $this->port;
        $this->http->on("start", function () use($ip,$port) {
            echo "Swoole http server is started at http://{$ip}:{$port}\n";
        });
        return $this;
    }

    private function request() 
    {
        $objRouter  = $this;
        $this->http->on('request', function ($request, $response) use($objRouter) 
        {
            $server = $request->server;
            $method = strtolower($server['request_method']);
            $uri    = $server['request_uri'];
            $body   = $request->rawContent();

            // pega a funcao atual
            //$callback = $pilha[0];

            // pega a ultima funcao
            //$next = $pilha[1];
            
            //$_response = call_user_func($callback,$request,$next);

            //$response->status($_response->statusCode);
            //$response->end($_response->message);

            
                
            /*
            if (isset($middleware[0]))
                $result = call_user_func($middleware[0]);

            if (isset($objRouter->getRoutes()[$method][$uri])) {
                $callback = $objRouter->getRoutes()[$method][$uri];
                $result = call_user_func($callback);
            }

            if (isset($middleware[1])) {
                $result = call_user_func($middleware[1]);
            }

            $response->end($result);*/

        });
        return $this;
    }

    private function start() 
    {
        $this->http->start();
        return $this;
    }

    public function listen() 
    {
        $this->up()->request()->start();
    }
}