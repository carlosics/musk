<?php

namespace Musk;

class Stack 
{
    protected $middlewares = [];

    protected $defaultResponse;

    public function __construct($response,...$middlewares)
    {
        $this->defaultResponse = $response;
        $this->middlewares = $middlewares;        
    }

    private function withoutMiddleware($middleware)
    {
        return new self(
            $this->defaultResponse,
            ...array_filter(
                $this->middlewares,
                function ($m) use ($middleware) {
                    return $middleware !== $m;
                }
            )
        );
    }

    public function handle($request)
    {
        $middleware = $this->middlewares[0] ?? false;

        $next = $this->withoutMiddleware($middleware);

        return $middleware
            ? $middleware->process(
                $request,
                $next   
            )
            : $this->defaultResponse;
    }    
}