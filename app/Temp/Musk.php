<?php

$app1 = new Application();

$app1->use(function(){
    return json_encode("\n executado antes");
});

$app1->on('GET','/teste', function() {
    // parei aqui. agora tem que criar o DI e o middleware.
    return (new UserController())->index(14,15,16);
});

$app1->use(function(){
    return json_encode("\n executado depois");
});

$app1->listen();