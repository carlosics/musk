<?php
declare(strict_types=1);

use Swoole\Http\Server;

error_reporting(E_ALL);

define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');


//Armazenar a lista de a��es a serem executadas antes e depois da a��o principal
//Dar ao middleware o poder de decidir se ele quer que paremos nele ou se vamos continuar para o pr�ximo.
//Executar os middlewares antes e depois da a��o principal (caso existam middlewares registrados).
//Executar os middlewares em ordem controlada.

class Router
{
    private $routes = [];
  
    public function on($method, $uri, $callback) 
    { 
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            $this->routes[$method] = [];
        }

        $uri = substr($uri, 0, 1) !== '/' ? '/' . $uri : $uri;
        //$pattern = str_replace('/', '\/', $uri);
        //$route = '/^' . $pattern . '$/';

        $this->routes[$method][$uri] = $callback;

        return $this;
    }
  
    public function run($method, $uri)
    {
        $method = strtolower($method);
        if (!isset($this->routes[$method])) {
            return null;
        }

        foreach ($this->routes[$method] as $route => $callback) {

            if (preg_match("#$route#", $uri, $parameters)) {
                array_shift($parameters);
                return call_user_func_array($callback, $parameters);
            }
        }
        return null;
    }

    public function getRoutes() 
    {
        return $this->routes;
    }
}

class OutPutJson
{
    public function json($json) 
    {
        return json_encode($json);
    }
}

class ControllerBase
{
    public $output;
    protected function json($data = []) 
    {
        return (new OutPutJson())->json($data);
    }
}

class UserController extends ControllerBase
{
    public function index() 
    {
        $params = func_get_args();

        return $this->json($params);
    }
}

class SSwoole extends Router
{
    public function __construct($ip = "0.0.0.0",$port = 9501)
    {
        $this->ip = $ip;
        $this->port = $port;
        $this->http = new Server($ip, $port);
        return $this;
    }

    private function up() 
    {
        $ip = $this->ip;
        $port = $this->port;
        $this->http->on("start", function () use($ip,$port) {
            echo "Swoole http server is started at http://{$ip}:{$port}\n";
        });
        return $this;
    }

    private function request() 
    {
        $pilha = [
            function($request, $next){
                echo "primeira";
                $next($request,$next);
            },
            function($request, $next){
                echo "segundo";
            },
            function($request, $next) {
                echo "ultimo";
            }
        ];
        $middleware = $this->middleware;
        $objRouter  = $this;
        // $this->getRoutes();
        $this->http->on('request', function ($request, $response) use($objRouter, $middleware, $pilha) 
        {
            $server = $request->server;
            $method = strtolower($server['request_method']);
            $uri    = $server['request_uri'];
            $body   = $request->rawContent();

            // pega a funcao atual
            //$callback = $pilha[0];

            // pega a ultima funcao
            //$next = $pilha[1];

            //call_user_func($callback,$request,$next);

            /*
            if (isset($middleware[0]))
                $result = call_user_func($middleware[0]);

            if (isset($objRouter->getRoutes()[$method][$uri])) {
                $callback = $objRouter->getRoutes()[$method][$uri];
                $result = call_user_func($callback);
            }

            if (isset($middleware[1])) {
                $result = call_user_func($middleware[1]);
            }

            $response->end($result);*/

        });
        return $this;
    }

    private function start() 
    {
        $this->http->start();
        return $this;
    }

    public function listen() 
    {
        $this->up()->request()->start();
    }
}

class Application extends SSwoole 
{
    protected $middleware = [];

    public function use($callback) 
    {
        $this->middleware[] = $callback;
    }

    public function __construct()
    {
        parent::__construct();
    }
}


class Mid1
{
    public function process($request, $handler)
    {
        echo "entrou mid1\n";
        return $handler->handle($request);
    }
}

class Middleware 
{
    protected $defaultResponse;
    protected $middlewares = [];

    public function __construct($defaultResponse,...$middlewares)
    {
        $this->defaultResponse = $defaultResponse;
        $this->middlewares = $middlewares;
    }

    private function withoutMiddleware($middleware)
    {
        echo $this->defaultResponse."\n";

        return new self($this->defaultResponse, ...array_filter(
                $this->middlewares,
                function ($m) use ($middleware) {
                    //var_dump($middleware);
                    //var_dump($m);
                    return $middleware !== $m;
                }
            )
        );
    }    

    public function handle($request)
    {
        $middleware = $this->middlewares[0] ?? false;

        // se middleware existe entao process
        return $middleware ? $middleware->process
        (
            $request,
            $this->withoutMiddleware($middleware)
        )
        : $this->defaultResponse;
    }
}

try {
    $request         = true;
    $defaultResponse = true;
    $stackResponse = new Middleware($defaultResponse,new Mid1());
    echo "RESPOSTA FINAL: \n";
    $stackResponse->handle($request);
    var_dump($stackResponse === $defaultResponse);

} catch(Exception $e) {
    echo $e->getMessage();
}



return;


$app1 = new Application();

$app1->use(function(){
    return json_encode("\n executado antes");
});

$app1->on('GET','/teste', function() {
    // parei aqui. agora tem que criar o DI e o middleware.
    return (new UserController())->index(14,15,16);
});

$app1->use(function(){
    return json_encode("\n executado depois");
});

$app1->listen();

return;

//https://github.com/mindplay-dk/middleman/tree/1.0.0
//https://route.thephpleague.com/4.x/middleware/
//https://phpsp.org.br/artigos/laravel-middlewares/
//double pass
//https://mwop.net/blog/2018-01-23-psr-15.html
// explica��o sobre middleware 
// https://pt.stackoverflow.com/questions/208114/qual-a-finalidade-do-middleware-em-rela%C3%A7%C3%A3o-a-apis-e-aplica%C3%A7%C3%B5es-web-feitas-no-sli

// https://github.com/idealo/php-middleware-stack/blob/master/src/Stack.php
// https://github.com/idealo/php-middleware-stack