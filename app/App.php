<?php

namespace Musk;

use Musk\Http\Server as Ht;
use Musk\Routing\Route as Rr;

class App
{
    private $before;
    private $after;

    public function __construct()
    {
        $this->router = new Rr;
        $this->httpServer = new Ht($this);
    }

    public function before(...$middleware)
    {
        if (!empty($middleware)) {
            $this->before = $middleware;
        }
        return $this;
    }

    public function after(...$middleware) 
    {
        if (!empty($middleware)) {
            $this->after = $middleware;
        }
        return $this;        
    }

    public function on($method,$uri,$callback,$middleware) 
    {
        $this->router->bind($method,$uri,$callback,$middleware);
        return $this;
    }

    public function listen()
    {
        $this->httpServer->listen();
        return $this;
    }

    public function dispacth($request, $response) 
    {
        // executa o rota
        $this->router->run(
            $this->before,
            $this->after,
            strtolower($request->server['request_method']),
            $request->server['request_uri'],
            $request,
            $response);
    }
}