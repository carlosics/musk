<?php

namespace Musk\Middleware;

class Middleware 
{
    public $middlewares = [];

    public $controller;

    public function __construct($controller,...$middlewares)
    {
        $this->controller  = $controller;
        $this->middlewares = $middlewares;
    }

    private function withoutMiddleware($middleware)
    {
        $proximos = array_filter(
            $this->middlewares,
            function ($m) use ($middleware) {
                return $middleware !== $m;
            }
        );
        return new self($this->controller,...$proximos);
    }

    public function handle($request,$response)
    {
        $controller = $this->controller;
        $middleware = $this->middlewares[0] ?? false;

        $next = $this->withoutMiddleware($middleware);

        if (!empty($middleware)) {
            return $middleware->process($request,$response,$next);
        }

        return $controller->process($request,$response,$next);
    }
}
