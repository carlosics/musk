<?php

namespace Musk\Http;

use Swoole\Http\Server as Ht;

use Musk\App;

class Server
{
    public function __construct(App $app,$ip = "0.0.0.0",$port = 9501)
    {
        $this->app  = $app;
        $this->ip   = $ip;
        $this->port = $port;
        $this->httpServer = new Ht($ip,$port);
        $this->setup();
        return $this;
    }

    private function setup() 
    {
        $ip   = $this->ip;
        $port = $this->port;
        $app  = $this->app;

        $this->httpServer->on("start", function () use($ip,$port) {
            echo "Swoole http server is started at http://{$ip}:{$port}\n";
        });

        $this->httpServer->on('request', function ($request, $response) use ($app)
        {
            $app->dispacth($request,$response);
        });

        $this->httpServer->on('close', function ($server, $fd) {
            echo "Connection closed: #{$fd}.\n";
        });
    }

    public function listen() 
    {
        $this->httpServer->start();
        return $this;
    }
}