<?php

namespace Musk\Http;

/**
 * Códigos de status de respostas HTTP
 * https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Status
 */

class Response
{
    public static function pageNotFound($response) 
    {
        $error = ["status"=>404,"message"=>"Page not found"];
        $response->code(404)->json($error);
    }
}