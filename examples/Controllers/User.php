<?php

namespace Examples\Controllers;

class User
{
    public function process($request,$response,$handler)
    {      
        $header = $request->getHeader();
        $server = $request->getServer();
        $get = $request->get();
        $post = $request->post();
        $cookie = $request->getCookie();
        $files = $request->getFiles();
        $body = $request->getBody();
        $getContent = $request->getContent();
        $data = $request->getData();

        return $response->text("User controllers");

        return $handler->handle($request,$response);
    }
}