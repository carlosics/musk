<?php

namespace Examples\Middlewares;

class Mid1 
{
    public function process($request,$response,$handler)
    {
        $params = (array) $request->get();

	    return $response->text("Entry in mid1: ". print_r($params, true));

        return $handler->handle($request,$response);
    }
}
