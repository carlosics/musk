<?php

namespace Examples\Middlewares;

class MidBefore
{
    public function process($request,$response,$handler)
    {
        $params = $request->get();

        if (!isset($params->id)) {
            //return $response->code(400)->text("Begin request");

            $response->transport($params->id);
        }

        return $handler->handle($request,$response);
    }
}