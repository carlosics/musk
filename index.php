<?php

declare(strict_types=1);

require_once "vendor/autoload.php";

use Examples\Controllers\User as UC;

use Examples\Middlewares\MidAfter as MA;

use Examples\Middlewares\MidBefore as MB;

use Examples\Middlewares\Mid1 as M1;

$app = (new Musk\App)

->before(new MB)

->on("GET" ,"/users", [new M1], new UC)
->on("GET" ,"/users2",[new M1], new UC)

->after(new MA)

->listen();